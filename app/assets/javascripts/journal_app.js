window.JournalApp = {
  Models: {},
  Collections: {},
  Views: {},
  Routers: {},
  initialize: function(data) {
    var posts = new JournalApp.Collections.Posts(data.posts)
    var postsIndex = new JournalApp.Views.PostsIndexView({collection: posts})

    $('.sidebar').append($(postsIndex.render().$el))
    new JournalApp.Routers.PostsRouter({$rootEl: $(".content"), posts: posts})
    Backbone.history.start();
  }
};

$(document).ready(function(){

});

