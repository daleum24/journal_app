class PostsController < ApplicationController
  respond_to :html, :json

  def index
    respond_with(@posts = Post.all)
  end

  def create
    @post = Post.new(params[:post])

    if @post.save
      respond_with @post
    else
      render json: @post.errors, status: 422
    end
  end

  def show
    @post = Post.find(params[:id])
  end

  def destroy
    Post.find(params[:id]).destroy
    head :ok
  end

  def update
    @post = Post.find(params[:id])

    if @post.update_attributes(params[:post])
      respond_with @post
    else
      render json: @post.errors, status: 422
    end
  end

  def new
    @post = Post.new(params[:post])
  end
end
