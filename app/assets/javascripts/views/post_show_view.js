JournalApp.Views.PostShowView = Backbone.View.extend({
  events: {
    "submit #post_form" : "update",
    "dblclick .title" : "editTitle",
    "dblclick .body" : "editBody",
    "blur .title" : "updatePost"
  },

  model: JournalApp.Models.Post,

  tagName: "ul",

  render: function() {
    this.$el.empty()

    var $currentTitle = $("<li class='title'>" + this.model.escape("title") + "</li>")
    var $currentBody  = $("<li class='body'>" + this.model.escape("body") + "</li>")

    var template = JST["post_form"]({
      model: this.model
    });

    this.$el.append($currentTitle).append($currentBody);
    this.$el.append($("</li><a href=#>Back</a>"))

    return this;
  },

  editTitle: function(event){
    var $title = $(event.currentTarget);

    var $text = $title.text()

    $title.remove()

    this.$el.prepend("<input class='title' type='text' value=" + $text + "></input>")

  },

  editBody: function(event){
    console.log("IN BOOTY");
  },

  remove: function() {
    this.$el.remove();
    this.stopListening();
    return this;
  },

  updatePost: function(event) {
    var that = this;
    var fieldValue = (event.currentTarget.className)
    var newValue = $(event.currentTarget).val()
    var formData = this.model.set({title: newValue})

    this.model.save({title: newValue},{
      success: function(model, response, options) {
        that.render()
      },
      error: function(model, xhr, options) {
        that.$el.empty();
        that.render()
      }
    })
  }
});