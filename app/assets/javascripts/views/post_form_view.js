JournalApp.Views.PostFormView = Backbone.View.extend({
  events: {"submit #post_form" : "create"},

  render: function() {
    this.$el.empty()

    var template = JST["post_form"]({
      model: this.model
    });

    this.$el.append(template);
    this.$el.append($("</li><a href=#>Back</a>"))

    return this;
  },

  remove: function() {
    this.$el.remove();
    this.stopListening();
    return this;
  },

  create: function(event){
    event.preventDefault();
    var that = this;
    var formData = $(event.currentTarget).serializeJSON();

    this.collection.create(formData.post,{
      success: function(model, response, options) {
        that.render()
      },
      error: function(model, xhr, options) {
        that.$el.empty();
        that.render()
      }
    })
  }
});