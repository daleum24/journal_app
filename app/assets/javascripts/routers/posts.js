JournalApp.Routers.PostsRouter = Backbone.Router.extend({
  initialize: function(options) {
    this.$rootEl = options.$rootEl;
    this.collection = options.posts;
  },

  routes: {
    "" : "index",
    "posts/:id" : "show",
    "new" : "new"
  },

  index: function() {
    var postsIndexView = new JournalApp.Views.PostsIndexView({collection: this.collection})

    // this._swapView(postsIndexView);
  },

  show: function(id) {
    var post = this.collection.get(id);

    var postShowView = new JournalApp.Views.PostShowView({model: post});

    this._swapView(postShowView);
  },

  new: function() {
    console.log("IN NEW")
    var newPost = new JournalApp.Models.Post();

    var postFormView = new JournalApp.Views.PostFormView({model: newPost, collection: this.collection})

    this._swapView(postFormView)
  },

  _swapView: function (newView) {
    this._currentView && this._currentView.remove();
    this._currentView = newView;
    this.$rootEl.html(newView.render().$el);
  }
})