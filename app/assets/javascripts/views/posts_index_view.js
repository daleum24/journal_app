JournalApp.Views.PostsIndexView = Backbone.View.extend({
  initialize: function(){
    var that = this;
    var events = ["remove", "add", "change:title", "reset"]
    _(events).each(function(event){
      that.listenTo(that.collection, event, that.render)
    });
  },
  tagName: "ul",

  render: function() {
    var that = this;

    this.$el.empty()

    this.collection.fetch();

    this.collection.forEach(function(post) {
      that.$el.append($("<li data-id=" + post.escape("id") + ">" + post.escape("title") + "</li><button class='delete_post' data-id=" + post.escape("id") + ">Delete</button>"))
    })

    return this;
  },
  remove: function() {
    this.$el.remove();
    this.stopListening();
    return this;
  },

  events: {
    "click .delete_post" : "deletePost",
    "click li" : "showPost"
  },

  showPost: function(event){
    event.preventDefault();

    var post_id = $(event.currentTarget).attr("data-id");

    var url = "posts/" + post_id
    // console.log(url)
    Backbone.history.navigate(url, {trigger:true});

  },

  deletePost: function(event) {
    event.preventDefault();
    var post_id = $(event.currentTarget).attr("data-id");
    var post = this.collection.get(post_id);

    post.destroy();
  }
})